export function sum(...numbersToSum: number[]) {
  return numbersToSum.reduce((acc, cur) => {
    return acc + cur;
  }, 0);
}

export function multiply(multiplier: number, ...numbersToMultiple: number[]) {
  return numbersToMultiple.map((number) => {
    return number * multiplier;
  });
}
