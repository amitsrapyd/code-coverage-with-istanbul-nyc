import { expect } from 'chai';
import { multiply, sum } from '../src/calc';

describe('calc module', () => {
  context('#sum', () => {
    it('should exist', () => {
      expect(sum).to.be.a('function');
      expect(sum).to.be.instanceOf(Function);
    });

    it('should work with two numbers', () => {
      expect(sum(2, 3)).to.equal(5);
    });
    it('should work with any amount of numbers', () => {
      expect(sum(2, 3, 9, 5)).to.equal(19);
    });
  });

  context('#multiply', () => {
    const delay = (ms: number) =>
      new Promise((resolve) => setTimeout(resolve, ms));

    it('should exist', () => {
      expect(multiply).to.be.a('function');
      expect(multiply).to.be.instanceOf(Function);
    });

    it('should work with two numbers', () => {
      expect(multiply(2, 3, 7)).to.deep.equal([6, 14]);
    });

    it('should work with any amount of numbers', () => {
      expect(multiply(2, 3, 7, 6, 4)).to.deep.equal([6, 14, 12, 8]);
    });

    it('should calculate after 1 second', async () => {
      await delay(500);
      expect(multiply(2, 3, 4)).to.deep.equal([6, 8]);
    });
  });
});
